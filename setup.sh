#!/bin/bash
############################
# .make.sh
# This script sets up customized dotfiles
############################

########## Variables

dir=~/dotfiles                    # dotfiles directory
olddir=~/dotfiles_old             # old dotfiles backup directory
files="bashrc zshrc oh-my-zsh vim vimrc tmux.conf p10k.zsh"    # list of files/folders to symlink in homedir

##########

# create dotfiles_old in homedir
echo -n "Creating $olddir for backup of any existing dotfiles in ~ ..."
mkdir -p $olddir
echo "done"

# change to the dotfiles directory
echo -n "Changing to the $dir directory ..."
cd $dir
echo "done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks from the homedir to any files in the ~/dotfiles directory specified in $files
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file ~/dotfiles_old/
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
done

install_zsh () {
# Test to see if zshell is installed.  If it is:
if [ -f /bin/zsh -o -f /usr/bin/zsh ]; then
    # Clone my oh-my-zsh repository from GitHub only if it isn't already present
    if [[ ! -d $dir/oh-my-zsh/ ]]; then
        git clone http://github.com/robbyrussell/oh-my-zsh.git
    fi
    # Set the default shell to zsh if it isn't currently set to zsh
    if [[ ! $(echo $SHELL) == $(grep /zsh$ /etc/shells | tail -1) ]]; then
    	echo "ZSH not default shell, changing to ZSH."
        chsh -s $(grep /zsh$ /etc/shells | tail -1)
    fi
else
    # If zsh isn't installed, get the platform of the current machine
    platform=$(uname);
    # If the platform is Linux, try an apt-get to install zsh and then recurse
    if [[ $platform == 'Linux' ]]; then
        if [[ -f /etc/redhat-release ]]; then
            sudo yum install zsh
            install_zsh
        fi
        if [[ -f /etc/debian_version ]]; then
            sudo apt-get install zsh
            install_zsh
        fi
    # If the platform is OS X, tell the user to install zsh :)
    elif [[ $platform == 'Darwin' ]]; then
        echo "Please install zsh, then re-run this script!"
        exit
    fi
fi
}

install_zsh

#Setup Update Check
echo "Setting up new Dotfiles auto-update"

echo "Exporting current crontab"
#write out current crontab
crontab -l > mycron

#echo new cron into cron file running every 1st minute of the hour
echo "Adding dotfile update job to crontab"
echo "1 */1 * * * ~/dotfiles/updates/autoupdate.sh" >> mycron

#install new cron file
crontab mycron
echo "Removing temp files"
rm mycron

#Create status file
echo "no" > ~/dotfiles/.dotfileupdatestatus
echo ""
echo "Setup complete!"


# Set Git Color
git config --global color.ui auto
