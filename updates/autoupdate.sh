#!/bin/bash
# Checks for dotfile updates

# Move to dotfiles directory
cd ~/dotfiles

# Grab update from Git
# Set timeout exit code
timeout 10 git fetch origin &>/dev/null

# Compares remote to local and changes update status if available.
if ! git diff --quiet origin/master; then
	echo "yes" > ~/dotfiles/.dotfileupdatestatus
fi