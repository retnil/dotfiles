#! /bin/zsh
#Checks to see if update is available for dotfiles and prompts the user to update if needed.

cd ~/dotfiles

# Load update status
dfstatus=$(<~/dotfiles/.dotfileupdatestatus)

# Check return value to see if there are incoming updates.
if [ "$dfstatus" = "yes" ]; then
	echo "Dotfile update is available. Would you like to update? [Y/n]: \c"
	read line
	if [[ "$line" == Y* ]] || [[ "$line" == y* ]] || [ -z "$line" ]; then
		# Use colors, but only if connected to a terminal, and that terminal supports them.
		if which tput >/dev/null 2>&1; then
		    ncolors=$(tput colors)
		fi
		if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
		  RED="$(tput setaf 1)"
		  GREEN="$(tput setaf 2)"
		  YELLOW="$(tput setaf 3)"
		  BLUE="$(tput setaf 4)"
		  BOLD="$(tput bold)"
		  NORMAL="$(tput sgr0)"
		else
		  RED=""
		  GREEN=""
		  YELLOW=""
		  BLUE=""
		  BOLD=""
		  NORMAL=""
		fi

		printf "${BLUE}%s${NORMAL}\n" "Updating Dotfiles..."
		if git pull --rebase --stat origin master
		then
		  printf '%s' "$GREEN"
		  printf '%s\n' '                                                     '
		  printf '%s\n' '                                                     '
		  printf "${BLUE}%s\n" "Awwww Yissssss! Dotfiles have been updated!"
		  printf '%s\n' '                                                     '
		  printf '%s\n' '                                                     '
		  echo "no" > ~/dotfiles/.dotfileupdatestatus
		else
		  printf "${RED}%s${NORMAL}\n" 'There was an error updating. Try again later?'
		fi
	else
	echo "Update deferred - will prompt again next time"
	fi
fi
#Cleanup unnecessary files and optimize the local repository 
git gc --auto --quiet